# ft_server

The aim of this project is set up a web server with nginx. It must run wordpress, phpMyAdmin and MySql. Basically, the steps are:

1. Crete a Dockerfile that run nginx;
2. Adds php in container config;
3. Set up wordpress;
4. Set up phpmyadmin and mysql;

There are some important points:
- Use SSL protocol;
- Set up autoindex;
- Forbidden docker composer;
- Only Dockerfile in the root, other files in srcs folder.


## Some useful commands

Building an image from dockerfile:

```docker build -t imageName .```

Building a container from created image:

```docker container run --name containerName -p 80:80 imageName```